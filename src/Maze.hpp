#ifndef _MAZE_H_
#define _MAZE_H_

#ifndef MAZE_VERSION
#define MAZE_VERSION __DATE__ " " __TIME__ " SNAPSHOT"
#endif // !MAZER_VERSION

#include <vector>
#include <string>

class Maze
{
public:
    static constexpr char wall = '*';
    static constexpr char path = ' ';
    static constexpr char step = 'x';
    static constexpr char exit = 'X';

    // vector<char> is fundementally a string
    using Grid = std::vector<std::string>;

    struct Cell
    {
        size_t row = 0, column = 0;

        Cell() {}
        Cell(size_t row, size_t column)
            : row(row), column(column)
        { }
    };

    bool isCellInOpenRange(Cell& c);
    bool isCellInClosedRange(Cell& c);
    bool isCellAWall(Cell& c);
    bool isCellAPath(Cell& c);
    bool isCellSteppedOn(Cell& c);
    bool isCellAnExit(Cell& c);

private:
    size_t columns, rows;
    Grid grid;

public:
    // Constructors
    Maze();
    Maze(size_t rows, size_t columns);
    Maze(Grid& grid);

    // member functions
    std::string str();
    void generate();
    bool solve();
    inline size_t getRows() { return rows; }
    inline size_t getColumns() { return columns; }
};

#endif // _MAZE_H_
