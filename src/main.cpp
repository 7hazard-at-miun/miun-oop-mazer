#include "Maze.hpp"

#include <cstdint>
#include <iostream>
#include <fstream>

#include <getopt.h>

#ifdef _WIN32
#include <Windows.h>
#endif // WIN32

#define ISODD(x) ((x%2) != 0)

constexpr const char* helpstr = R"(
Mazer Commands Help
--help | -h
    Outputs available arguments (this)
--version | -v
    Outputs version number
(--size | -s)N
    Constructs a maze with width and height N
(--columns | -c)W
    Constructs a maze by width W, overrules --size
(--rows | -r)H
    Constructs a maze by height H, overrules --size
(--input | -i)file
    Use file as input, disregards --size, --columns and --rows
(--output | -o)file
    Outputs results to file, otherwise stdout
--check | -b
    Outputs solving to stdout either as 
    "Solution found" or "Solution not found"

Arguments order matter
help(-h) argument will not perform any maze generation or solving
Maze solving will be performed when a valid --input(-i) is specified
)";

static int verbose_flag;

int main(int argc, char *argv[])
{
    // Skriva ut svenska bokst�ver i konsolen
#ifdef _WIN32
    SetConsoleOutputCP(1252);
#endif // _WIN32

    //Option::ProcessAll(argc, argv);

    int c;
    size_t rows = 0, columns = 0;
    std::ifstream infile;
    std::ofstream outfile;
    bool checkonly = false;

    while (1)
    {
        static struct option long_options[] =
        {
            /* These options set a flag. */
            //{"verbose", no_argument,       &verbose_flag, 1},
            //{"brief",   no_argument,       &verbose_flag, 0},
            /* These options don�t set a flag.
               We distinguish them by their indices. */
            {"version",    no_argument, 0, 'v'},
            {"help",    no_argument, 0, 'h'},
            {"size",    required_argument, 0, 's'},
            {"columns",    required_argument, 0, 'c'},
            {"rows",    required_argument, 0, 'r'},
            {"input",    required_argument, 0, 'i'},
            {"output",    required_argument, 0, 'o'},
            {"check",    no_argument, 0, 'b'},
            {0, 0, 0, 0}
        };

        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "vhbs:c:r:i:o:",
            long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c)
        {
        case 0:
        {
            /* If this option set a flag, do nothing else now. */
            if (long_options[option_index].flag != 0)
                break;
            printf("option %s", long_options[option_index].name);
            if (optarg)
                printf(" with arg %s", optarg);
            printf("\n");
            break;
        }
        case 'v': // version
        {
            std::cout << "Mazer version '" << MAZE_VERSION << "'" << std::endl;
            break;
        }
        case 'h': // help
        {
            std::cout << helpstr << std::endl;
            return EXIT_SUCCESS;
        }
        case 's': // size
        {
            try
            {
                rows = std::stoi(optarg);
                columns = std::stoi(optarg);
            }
            catch (const std::exception&)
            {
                std::cerr << "Could not convert --size(-c) to a reasonable integer" << std::endl;
                return EXIT_FAILURE;
            }
            break;
        }
        case 'c': // columns
        {
            try
            {
                columns = std::stoi(optarg);
            }
            catch (const std::exception&)
            {
                std::cerr << "Could not convert --columns(-c) to a reasonable integer" << std::endl;
                return EXIT_FAILURE;
            }
            break;
        }
        case 'r': // rows
        {
            try
            {
                rows = std::stoi(optarg);
            }
            catch (const std::exception&)
            {
                std::cerr << "Could not convert --rows(-r) to a reasonable integer" << std::endl;
                return EXIT_FAILURE;
            }
            break;
        }
        case 'i': // input
        {
            infile = std::ifstream(optarg);
            if (!infile.good())
            {
                std::cerr << "Could not open file " << optarg << std::endl;
                return EXIT_FAILURE;
            }
            else
            {
                std::cout << "Reading from file " << optarg << std::endl;
            }
            break;
        }
        case 'o': // output
        {
            outfile = std::ofstream(optarg);
            if (!outfile.good())
            {
                std::cerr << "Could not write to file " << optarg << std::endl;
                return EXIT_FAILURE;
            }
            else
            {
                std::cout << "Writing to file " << optarg << std::endl;
                outfile.clear();
            }
            break;
        }
        case 'b': // check
        {
            checkonly = true;
            break;
        }
        case '?':
            /* getopt_long already printed an error message. */
            break;

        default:
            abort();
        }
    }
    std::cout << std::endl;

    Maze maze;
    if (infile.is_open())
    {
        Maze::Grid grid;
        std::string line;
        while (std::getline(infile, line))
        {
            if (line.empty())
                break;
            grid.push_back(line);
        }

        try
        {
            grid[0];
        }
        catch (const std::exception&)
        {
            std::cerr << "Maze in input file is malformed" << std::endl;
            return EXIT_FAILURE;
        }

        maze = Maze(grid);
        std::cout << "Rows: " << maze.getRows() 
            << ", Columns: " << maze.getColumns() << std::endl;

        bool solved = maze.solve();
        if (checkonly)
        {
            solved
                ? std::cout << "Solution found" << std::endl
                : std::cout << "Solution not found" << std::endl;
        }
        else if (outfile.is_open())
        {
            if(solved)
                outfile << maze.str();
        }
        else
        {
            if(solved)
                std::cout << maze.str() << std::endl;
        }
    }
    else if (rows || columns)
    {
        std::cout << "Generating " << rows << "x" << columns 
            << " maze" << std::endl;

        if (!ISODD(rows) || !ISODD(columns) || rows < 3 || columns < 3)
        {
            std::cerr
                << "--size(-s), --rows(-r) and --columns(-c) has to be at least 3 " << std::endl
                << "    and has to be odd values larger than 0" << std::endl;

            return EXIT_FAILURE;
        }
        
        maze = Maze(rows, columns);

        maze.generate();

        if (outfile.is_open())
            outfile << maze.str();
        else
            std::cout << maze.str() << std::endl;
    }

    infile.close();
    outfile.close();

    return EXIT_SUCCESS;
}