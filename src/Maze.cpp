﻿#include "Maze.hpp"

#include <stack>
#include <algorithm>
#include <chrono>
#include <random>
#include <vector>
#include <iostream>

Maze::Maze()
{ }

Maze::Maze(Grid& grid)
    : grid(grid), rows(grid.size()), columns(grid[0].size())
{ }

Maze::Maze(size_t rows, size_t columns)
    : rows(rows), columns(columns)
{
    // grid[rows][columns]
    // make all cells walls
    grid.resize(rows);
    for (size_t i = 0; i < rows; i++)
    {
        grid[i].resize(columns);

        // Put wall on cell
        for (size_t j = 0; j < columns; j++)
        {
            grid[i][j] = wall;
        }
    }
}

std::string Maze::str()
{
    std::string str;
    for (size_t i = 0; i < rows; i++)
    {
        str += grid[i]+'\n';
    }

    return str;
}

bool Maze::isCellInOpenRange(Cell& c)
{
    return c.row > 0 && c.row < rows
        && c.column > 0 && c.column < columns;
}

bool Maze::isCellInClosedRange(Cell& c)
{
    return c.row > 0 && c.row < rows - 1
        && c.column > 0 && c.column < columns - 1;
}

bool Maze::isCellAWall(Cell& c)
{
    if (!isCellInClosedRange(c))
        return false;
    else
        return grid[c.row][c.column] == wall;
}

bool Maze::isCellAPath(Cell& c)
{
    if (!isCellInClosedRange(c))
        return false;
    else
        return grid[c.row][c.column] == path;
}

bool Maze::isCellSteppedOn(Cell& c)
{
    if (!isCellInClosedRange(c))
        return false;
    else
        return grid[c.row][c.column] == step;
}

bool Maze::isCellAnExit(Cell& c)
{
    if (!isCellInOpenRange(c))
        return false;
    else
        return grid[c.row][c.column] == exit;
}

bool operator==(const Maze::Cell& l, const Maze::Cell& r)
{
    return (l.row == r.row) && (l.column == r.column);
}

// https://en.wikipedia.org/wiki/Maze_generation_algorithm
void Maze::generate()
{
    // First in pair is the neighbors cell
    // second is the wall between
    using Neighbor = std::pair<Cell, Cell>;

    std::stack<Cell> s;
    std::vector<Cell> v;
    Cell cc;

    // lambda function relative to this function
    // think of it as a nested function
    auto isUnvisited = [&](Cell& c) {
        // https://stackoverflow.com/questions/571394/how-to-find-out-if-an-item-is-present-in-a-stdvector
        return std::find(v.begin(), v.end(), c) == v.end();
    };

    // lambda function
    auto getuvn = [&]() {
        // Maximum uvn is 3
        std::vector<Neighbor> uvn; uvn.reserve(3);

        Neighbor left = { { cc.row, cc.column - 2 }, { cc.row, cc.column - 1 } },
            up = { { cc.row - 2, cc.column } , { cc.row - 1, cc.column } },
            right = { { cc.row, cc.column + 2 }, { cc.row, cc.column + 1 } },
            down = { { cc.row + 2, cc.column }, { cc.row + 1, cc.column } };

        // Check if cells are in range and unvisited
        // if so, add them to uvn list

        // Left
        if (isCellInClosedRange(left.first) && isUnvisited(left.first))
            uvn.push_back(left);
        // up
        if (isCellInClosedRange(up.first) && isUnvisited(up.first))
            uvn.push_back(up);
        // right
        if (isCellInClosedRange(right.first) && isUnvisited(right.first))
            uvn.push_back(right);
        // down
        if (isCellInClosedRange(down.first) && isUnvisited(down.first))
            uvn.push_back(down);

        // shuffle
        unsigned seed = std::chrono::system_clock::now()
            .time_since_epoch().count();
        std::shuffle(uvn.begin(), uvn.end(), std::default_random_engine(seed));
        return uvn;
    };

    // Set start at 1, 1
    // make current
    cc = { 1, 1 };
    grid[1][0] = 'S';

    // Push current to stack, set visited
    s.push(cc);
    v.push_back(cc);

    // while stack is not empty
    while (!s.empty())
    {
        // Get unvisited neighbors of current cell in a random order (shuffle)
        auto uvn = getuvn();

        // if there are unvisited neightbors
        if (uvn.size() > 0)
        {
            // get any neighbor
            // gets neighbor and wall between
            auto[n, w] = uvn[0];

            // make path to neighbor
            grid[w.row][w.column] = path;
            grid[n.row][n.column] = path;

            // set and push current to stack, set visited
            cc = n;
            s.push(cc);
            v.push_back(cc);
        }
        else
        {
            // Pop stack into current
            cc = s.top();
            s.pop();
        }
    }

    // Set start and exit point
    grid[1][1] = path;
    grid[rows-2][columns-1] = 'X';
}

bool Maze::solve()
{
    bool solved = false;
    std::stack<Cell> s;
    std::vector<Cell> v;
    Cell cc;

    // lambda function relative to this function
    // think of it as a nested function
    auto isUnvisited = [&](Cell& c) {
        // https://stackoverflow.com/questions/571394/how-to-find-out-if-an-item-is-present-in-a-stdvector
        return std::find(v.begin(), v.end(), c) == v.end();
    };

    // lambda function
    auto isCellValid = [&](Cell& c) {
        return (isCellInClosedRange(c)
            && isCellAPath(c)
            && isUnvisited(c))
            || isCellAnExit(c);
    };

    // lambda function
    auto getuvn = [&]() {
        // Maximum uvn is 3
        std::vector<Cell> uvn; uvn.reserve(3);

        Cell left = { cc.row, cc.column - 1 },
            up = { cc.row - 1, cc.column },
            right = { cc.row, cc.column + 1 },
            down = { cc.row + 1, cc.column };

        // Check if cells are in range and unvisited
        // if so, add them to uvn list

        // right
        if (isCellValid(right)) return right;
        // down
        if (isCellValid(down)) return down;
        // Left
        if (isCellValid(left)) return left;
        // up
        if (isCellValid(up)) return up;

        return Cell{ 0, 0 };
    };

    // Set start at 1, 1
    // make current
    cc = { 1, 1 };

    // Do not attempt to solve if start is walled
    if (!isCellValid(cc))
        return false;

    // push to stack
    s.push(cc);
    v.push_back(cc);

    while (!s.empty())
    {
        // Get a random unvisited neighbor of current cell
        auto uvn = getuvn();

        // if there are unvisited neightbors
        if (uvn == Cell{0, 0})
        {
            // Pop stack into current
            cc = s.top();
            s.pop();
        }
        // if is exit cell
        else if (isCellAnExit(uvn))
        {
            solved = true;
            break;
        }
        else
        {
            // push current to stack, set visited
            s.push(cc);
            // get any neighbor, set current, push to stack
            cc = uvn;
            s.push(cc);
            v.push_back(cc);
        }
    }

    // paint way to solution
    while (!s.empty())
    {
        grid[cc.row][cc.column] = step;
        cc = s.top();
        s.pop();
    }

    return solved;
}
